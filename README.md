# SmarterWidgets
## Summary

SmarterWidgets is a set of extensions built for the needs of IBM Engineering platform users (DOORS Next, Test Management, Global Configuration), which are intended to facilitate everyday work or more complex work requiring automation.

The set of extensions is still being developed so the list of available extensions will be constantly growing

## List of widgets

1. TermMap - Create references between glossary terms and artefacts.

### TermMap

Create references between glossary terms and artefacts.

#### Version: Version 4 (release date 15.2.2021)

#### Description:

Many customers working with DOORS Next face a challenge where they have requirements documents in the product and hundreds or thousands of definitions in the dictionaries. Linking these elements together as a relationship in a manual way will be a process that will take a lot of time and may lead to errors.

The IBM DOORS Next product widget automatically retrieves a list of available definitions from the project and then creates a relationship between the artifact and the definition by creating a link and hyperlink in the text.

The widget allows to create references for requirements selected by the user or to do this work for the whole module with requirements.


#### Use:

**Step 1:**

In step 1 user is asked to define artifact type of glossary term (default: Term) in the widget properties.

Next user should select terms in the project that will be linked to the artefact later or can user can use option to find all glossay terms in the project that will return complete list of terms.

**Step 2:**

#### Simple Mode
Link selected artifact(s) to the glossary terms from step 1.

#### Module Mode
Link all artifact(s) within entire module to the glossary terms from step 1. Mode is enabled only when module is open. In this mode the reference links are created in base context.

#### Contact (questions):

Bartosz Chrabski
<info@reqpro.com>
<info@smarterprocess.pl>