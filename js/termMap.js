/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2020
 * Contact information: info@reqpro.com
 */

// Napisac do Ann na temat zmian
// - Wyszukiwanie w modulach odpowiednio odblokowuje guziki

var moduleRef;

var parameterName;

var debugMode = false;

var enabled = false;

var parameters = [];

var runningProcess = false;

function sortByLength(a, b) {
  var aText = a.values[RM.Data.Attributes.NAME];
  var bText = b.values[RM.Data.Attributes.NAME];
  {
    if (aText.length < bText.length) {
      return 1;
    }
    if (aText.length > bText.length) {
      return -1;
    }

    return 0;
  }
}

$(function () {
  if (window.RM) {
    // *** DEBUG MODE
    if (debugMode == "true") console.group("Initialization");
    if (debugMode == "true") console.log("Debug Mode ... Function -> Widget initialization Start");
    if (debugMode == "true") console.groupEnd("Initialization");
    // ***

    var prefs = new gadgets.Prefs();
    parameterName = prefs.getString("parameterName");
    debugMode = prefs.getString("debugMode");

    // *** DEBUG MODE
    if (debugMode == "true") console.group("Initialization");
    if (debugMode == "true") console.log("Debug Mode ... Debug mode enabled: " + debugMode);
    if (debugMode == "true") console.groupEnd("Initialization");

    // ***

    // *** DEBUG MODE
    if (debugMode == "true") console.group("Initialization");
    if (debugMode == "true") console.log("Debug Mode ... Function -> Widget initialization End");
    if (debugMode == "true") console.groupEnd("Initialization");
    // ***

    $("#selectTermsButton").on("click", function () {
      selectGlossaryTerms();
    });

    $("#findAllTerms").on("click", function () {
      findAllGlossaryTerms();
    });

    gadgets.window.adjustHeight();

    //
  } else {
    clearSelectedInfo();
    lockAllButtons();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    gadgets.window.adjustHeight();
  }
});

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, async function (selected) {
  var outcome = "";
  if (selected.length === 1) {
    // *** DEBUG MODE ***
    if (debugMode == "true") console.group("Selection");
    if (debugMode == "true") console.log("Debug Mode ... Single artifact selected");
    if (debugMode == "true") console.groupEnd("Selection");
    // ***

    if (enabled == false) return;
    unlockButton("#actionButton");
    $("#actionButton").unbind();
    $(".status").removeClass("warning correct incorrect");
    $("#actionButton")
      .css("display", "inline")
      .on("click", async function () {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");

        process(selected);
      });
    $(".status").removeClass("warning correct incorrect");
    $(".status").html("<b>Message:</b> One artifact is selected.");
  } else if (selected.length > 1) {
    // *** DEBUG MODE
    if (debugMode == "true") console.group("Selection");
    if (debugMode == "true") console.log("Debug Mode ... Multiple artifacts selected");
    if (debugMode == "true") console.groupEnd("Selection");
    //****
    if (enabled == false) return;
    unlockButton("#actionButton");
    $("#actionButton").unbind();
    $(".status").removeClass("warning correct incorrect");
    $("#actionButton")
      .css("display", "inline")
      .on("click", async function () {
        lockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");
        process(selected);
      });
    $(".status").html(`<b>Message:</b> ${selected.length} artifacts are selected.`);
  } else {
    // *** DEBUG MODE
    if (debugMode == "true") console.group("Selection");
    if (debugMode == "true") console.log("Debug Mode ... Zero artifacts selected");
    if (debugMode == "true") console.groupEnd("Selection");
    //**
    // clear the display area...
    lockButton("#actionButton");
    $("#actionButton").unbind();
    clearSelectedInfo();
    // inform the user that they need to select only one thing.
    $(".status").html("<b>Message:</b> Select one or multiple objects.");
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {

  if (enabled == false) return;
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {

  $("#actionInModuleButton").off("click");

  $("#actionInModuleButton").attr("disabled", "disabled");
  $("#actionInModuleButton").removeClass("btn-primary");
  $("#actionInModuleButton").addClass("btn-secondary");
  moduleRef = null;
});

async function process(selected) {
  var outcome = "";
  var respose;

  parameters.sort(sortByLength);

  return new Promise(async function (resolve, reject) {
    for (var q = 0; q < selected.length; q++) {
      var obj = selected[q]; // tutaj
      var uriTmp = uriToPublishURI(obj.uri);
      if (uriTmp) {
        let tmp2 = new RM.ArtifactRef(uriTmp, obj.componentUri, null, obj.format);
        obj = tmp2;
      }
      let usedParameters = [];

      var opResult = await getAttributes(obj); // tutaj

      if (opResult.code !== RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
        // poprawka
      } else {
        lockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status")
          .addClass("warning")
          .html(`<b>Message:</b> Processing artifact ${q + 1} out of ${selected.length}.`);
        var toSave = [];

        for (artAttrs of opResult.data) {
          // Reset some attribute values
          var tmp = artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT];

          console.log(artAttrs.values[RM.Data.Attributes.IS_HEADING]);

          for (item of parameters) {
            // tutaj

            var name = item.values[RM.Data.Attributes.NAME]; // tutaj

            var nameReg = escapeRegExp(name);
            var replace = "(?!([^<]+)?>)(?!<a[^>]*?>)(?<![a-z0-9A-Z]+)(" + nameReg + ")(?![a-z0-9A-Z]+)(?![^<]*?</a>)";

            var re = new RegExp(replace, "g");

            // Implementacja tego usuwania

            if (tmp.match(re)) {
              usedParameters.push(item); // tutaj
            }

            tmp = tmp.replace(re, "<a href='" + item.ref.uri + "'>" + name + "</a>"); // tutaj
          }
          artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT] = tmp;
          toSave.push(artAttrs);
        }

        var z = await modifyArtifacts(toSave);
        respose = z.code;
      }

      if (respose != RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
      } else {
        for (const link of usedParameters) {
          var result = await createTermLink(obj, link.ref); // tutaj
          if (result.code != RM.OperationResult.OPERATION_OK) {
            // outcome = result.code + " " + result.message;
          }
        }
      }
    }

    if (outcome.length == 0) {
      unlockAllButtons();
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("correct").html("<b>Success:</b> Terms referenced in selected artifacts(s).");
    } else {
      {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html(`<b>Warning:</b> ${outcome}.`);
      }
    }
    resolve("");
  });
}

function checkIfModule(opResult) {


  if (opResult.code === RM.OperationResult.OPERATION_OK) {

    var attrs = opResult.data[0];

    if (attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE ||
      attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.COLLECTION) {
      moduleRef = attrs.ref;
      if (moduleRef!=attrs.ref)
      {
        moduleRef = attrs.ref;
        unlockButton("#actionInModuleButton");
        $("#actionInModuleButton").on("click", verifyArtifactsInModule);
      }
    }
  }

}

async function processForModule() {
  lockAllButtons();

  $(".status").removeClass("warning correct incorrect");
  $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");

  var result = await getModuleArtifacts(moduleRef);

  if (result.code !== RM.OperationResult.OPERATION_OK) {
    unlockButton("#actionInModuleButton");
    unlockButton("#selectTermsButton");
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
  } else {
    var tmp = [];
    for (item of result.data) {
      tmp.push(item.ref);
    }
    process(tmp);
  }
}

function clearSelectedInfo() {
  // *** DEBUG MODE
  if (debugMode == "true") console.group("removeAllTermsFromModule");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Clear selected info Start");
  if (debugMode == "true") console.groupEnd("removeAllTermsFromModule");
  // ***

  $(".status").removeClass("incorrect correct warning").html("");

  $(".selected, .error").removeClass("selected error");
  $(".setRPNButton").css("display", "none");

  // *** DEBUG MODE
  if (debugMode == "true") console.group("removeAllTermsFromModule");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Clear selected info End");
  if (debugMode == "true") console.groupEnd("removeAllTermsFromModule");
  // ***
}

function selectGlossaryTerms() {
  // *** DEBUG MODE
  if (debugMode == "true") console.group("selectGlossaryTerms");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Find all paramters Start");
  if (debugMode == "true") console.groupEnd("selectGlossaryTerms");
  // ***
  enabled = false;
  parameters = [];
  $(".status").removeClass("warning correct incorrect");
  $(".status").addClass("warning").html("<b>Message:</b> Select glossary terms");

  lockAllButtons();

  RM.Client.getCurrentConfigurationContext(function (result) {
    // *** DEBUG MODE
    if (debugMode == "true") console.group("selectGlossaryTerms");
    if (debugMode == "true") console.log("Debug Mode ... Getting information about current configuration context");
    if (debugMode == "true") console.groupEnd("selectGlossaryTerms");
    // ***
    if (result.code === RM.OperationResult.OPERATION_OK) {
      // *** DEBUG MODE
      if (debugMode == "true") console.group("selectGlossaryTerms");
      if (debugMode == "true") console.log("Debug Mode ... Got information about current configuration context ");
      if (debugMode == "true") console.groupEnd("selectGlossaryTerms");

      // ***
      var context = result.data;
      var componentURL = context.localComponentUri;
      var configurationURL = context.localConfigurationUri;
      var projectURL = getProjectAreaURL(componentURL);
      var serverURL = projectURL.split("/process/project-areas/")[0];

      // *** DEBUG MODE

      if (debugMode == "true") console.group("selectGlossaryTerms");
      if (debugMode == "true") console.log("Debug Mode ... Getting parameter type URL");
      if (debugMode == "true") console.groupEnd("selectGlossaryTerms");
      // ***
      var parameterTypeURL = getParametersURL(componentURL, projectURL, configurationURL);

      // *** DEBUG MODE
      if (debugMode == "true") console.group("selectGlossaryTerms");
      if (debugMode == "true") console.log("Debug Mode ... Getting link type URL");
      if (debugMode == "true") console.groupEnd("selectGlossaryTerms");

      // ***

      if (parameterTypeURL == undefined) {
        // *** DEBUG MODE
        if (debugMode == "true") console.group("selectGlossaryTerms");

        if (debugMode == "true") console.log("Debug Mode ... Parameter type URL not available");

        if (debugMode == "true") console.groupEnd("selectGlossaryTerms");
        // ***
        $(".status").removeClass("warning correct incorrect");

        unlockButton("#selectTermsButton");

        $(".status")
          .addClass("warning")
          .html(`<b>Warning:</b> Artifact type ${parameterName} not found or ${parameterName} is not a glossary term.`);
        // *** DEBUG MODE
        if (debugMode == "true") console.group("selectGlossaryTerms");
        if (debugMode == "true") console.log("Debug Mode ... Function -> Find all paramters End");
        if (debugMode == "true") console.groupEnd("selectGlossaryTerms");
        // ***
        return;
      }

      // *** DEBUG MODE

      if (debugMode == "true") console.group("selectGlossaryTerms");
      if (debugMode == "true") console.log("Debug Mode ... Selecting glossary terms");
      if (debugMode == "true") console.groupEnd("selectGlossaryTerms");
      // ***
      // OTWIERANIE

      RM.Client.showArtifactPicker(function (pickResult) {
        if (pickResult.code == "OK") {
          all = [];
          paramters = [];
          RM.Data.getAttributes(
            pickResult.data,
            [RM.Data.Attributes.NAME, RM.Data.Attributes.ARTIFACT_TYPE],
            function (attrResult) {
              for (attrValue of attrResult.data) {
                if (attrValue.values[RM.Data.Attributes.ARTIFACT_TYPE].name == parameterName) {
                  parameters.push(attrValue);
                }
              }

              $("#gtNumber").html(parameters.length);

              if (parameters.length > 0) {
                enabled = true;
                parameters.sort(sortByLength);
                for (s of parameters) {
                  console.log(s);
                }
                $(".status").removeClass("warning correct incorrect");
                $(".status")
                  .addClass("correct")
                  .html(`<b>Message:</b> Selected ${parameters.length} glossary terms of ${parameterName} type.`);

                unlockButton("#selectTermsButton");
                unlockButton("#findAllTerms");

                if (moduleRef) {
                  unlockButton("#actionInModuleButton");
                }
              } else {
                $(".status").removeClass("warning correct incorrect");
                $(".status")
                  .addClass("warning")
                  .html(`<b>Message:</b> None of selected artifacts was type of ${parameterName}.`);
                unlockButton("#selectTermsButton");
                unlockButton("#findAllTerms");
              }
            }
          );
        } else {
          $("#gtNumber").html("0");
          $(".status").removeClass("warning correct incorrect");
          $(".status")
            .addClass("warning")
            .html(`<b>Message:</b> None of selected artifacts was type of ${parameterName}.`);
          unlockButton("#selectTermsButton");
          unlockButton("#findAllTerms");
        }

        $("#gtNumber").html(parameters.length);
      });
    }
  });

  // *** DEBUG MODE
  if (debugMode == "true") console.group("findAllParameter");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Find all paramters End");
  if (debugMode == "true") console.groupEnd("findAllParameter");
  // ***
}

async function findAllGlossaryTerms() {
  // ***
  enabled = false;
  parameters = [];
  $(".status").removeClass("warning correct incorrect");
  $(".status").addClass("warning").html("<b>Message:</b> Searching glossary terms");

  lockAllButtons();

  RM.Client.getCurrentConfigurationContext(async function (result) {
    all = [];
    parameters = [];

    // *** DEBUG MODE
    if (debugMode == "true") console.group("findAllGlossaryTerms");
    if (debugMode == "true") console.log("Debug Mode ... Getting information about current configuration context");
    if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
    // ***
    if (result.code === RM.OperationResult.OPERATION_OK) {
      // *** DEBUG MODE
      if (debugMode == "true") console.group("findAllGlossaryTerms");
      if (debugMode == "true") console.log("Debug Mode ... Got information about current configuration context ");
      if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");

      // ***
      var context = result.data;
      var componentURL = context.localComponentUri;
      var configurationURL = context.localConfigurationUri;
      var projectURL = getProjectAreaURL(componentURL);
      var serverURL = projectURL.split("/process/project-areas/")[0];

      // *** DEBUG MODE

      if (debugMode == "true") console.group("findAllGlossaryTerms");
      if (debugMode == "true") console.log("Debug Mode ... Getting parameter type URL");
      if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
      // ***
      var parameterTypeURL = getParametersURL(componentURL, projectURL, configurationURL);

      // *** DEBUG MODE
      if (debugMode == "true") console.group("findAllGlossaryTerms");
      if (debugMode == "true") console.log("Debug Mode ... Getting link type URL");
      if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");

      // ***

      if (parameterTypeURL == undefined) {
        // *** DEBUG MODE
        if (debugMode == "true") console.group("findAllGlossaryTerms");

        if (debugMode == "true") console.log("Debug Mode ... Parameter type URL not available");

        if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
        // ***
        $(".status").removeClass("warning correct incorrect");
        // tutaj zmiana
        unlockButton("#selectTermsButton");
        unlockButton("#findAllTerms");

        $(".status")
          .addClass("warning")
          .html(`<b>Warning:</b> Artifact type ${parameterName} not found or ${parameterName} is not a glossary term.`);
        // *** DEBUG MODE
        if (debugMode == "true") console.group("findAllGlossaryTerms");
        if (debugMode == "true") console.log("Debug Mode ... Function -> Find all paramters End");
        if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
        // ***
        return;
      }

      // *** DEBUG MODE

      if (debugMode == "true") console.group("findAllGlossaryTerms");
      if (debugMode == "true") console.log("Debug Mode ... Selecting glossary terms");
      if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
      // ***

      // OTWIERANIE

      var query =
        serverURL +
        "/views?oslc.query=true&projectURL=" +
        projectURL +
        "&oslc.prefix=dcterms=" +
        encodeURIComponent("<http://purl.org/dc/terms/>") +
        "&oslc.prefix=rm=" +
        encodeURIComponent("<http://www.ibm.com/xmlns/rdm/rdf/>") +
        "&oslc.prefix=nav=" +
        encodeURIComponent("<http://jazz.net/ns/rm/navigation#>") +
        "&oslc.where=rm:ofType=<" +
        parameterTypeURL +
        ">&oslc.select=nav:parent&oslc.select=dcterms:title";

      var parameter_req = new XMLHttpRequest();

      parameter_req.open("GET", query, false);
      parameter_req.setRequestHeader("Accept", "application/rdf+xml");
      parameter_req.setRequestHeader("Content-Type", "application/rdf+xml");
      parameter_req.setRequestHeader("OSLC-Core-Version", "2.0");
      parameter_req.setRequestHeader("Configuration-Context", configurationURL);

      try {
        parameter_req.send();
        // *** DEBUG MODE
        if (debugMode == "true") console.group("findAllGlossaryTerms");
        if (debugMode == "true") console.trace("Debug Mode ... Got all list of all the parameters");
        if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
        // ***

        var objects = parameter_req.responseXML.getElementsByTagNameNS(
          "http://open-services.net/ns/rm#",
          "Requirement"
        );

        for (i = 0; i < objects.length; i++) {
          var object = objects[i];

          var title = object.getElementsByTagNameNS("http://purl.org/dc/terms/", "title")[0].innerHTML;
          var about = object.getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");
          var parent = object.getElementsByTagNameNS("http://jazz.net/ns/rm/navigation#", "parent");

          if (parent.length > 0) {
            let component = componentURL.split("/cm/component/")[1];

            let tmpURL = projectURL.replace("process/project-areas", "rm-projects");
            tmpURL = tmpURL + "/components/" + component;
            console.log(tmpURL);
            let tmp2 = new RM.ArtifactRef(
              about,
              tmpURL,
              null,
              "http://www.ibm.com/xmlns/rdm/types/ArtifactFormats#Text"
            );
            parameters.push(tmp2);
          }

          //
        }

        var opResult = await getAttributes(parameters);

        if (opResult.code !== RM.OperationResult.OPERATION_OK) {
          parameters = [];
        } else {
          parameters = opResult.data;
        }

        $("#gtNumber").html(parameters.length);

        if (parameters.length > 0) {
          // *** DEBUG MODE
          parameters.sort(sortByLength);
          if (debugMode == "true") console.group("findAllGlossaryTerms");
          if (debugMode == "true") console.trace(`Debug Mode ... Found ${parameters.length} one parameter(s)`);
          if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
          // ***
          enabled = true;
          $("#status").removeClass("warning correct incorrect");
          $("#status")
            .addClass("correct")
            .html(`<b>Success:</b> Found ${parameters.length} term(s). You can move to step 2.`);
          unlockButton("#selectTermsButton");
          unlockButton("#findAllTerms");
          if (moduleRef) {
            unlockButton("#actionInModuleButton");
          }
          return;
        } else {
          // *** DEBUG MODE

          if (debugMode == "true") console.group("findAllGlossaryTerms");
          if (debugMode == "true") console.trace("Debug Mode ... Did not find one parameter(s)");
          if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
          // ***
          $("#status").removeClass("warning correct incorrect");
          $("#status").addClass("warning").html(`<b>Warning:</b> Term(s) were not found`);
          unlockButton("#selectTermsButton");
          unlockButton("#findAllTerms");
          return;
        }
      } catch (err) {
        if (debugMode == "true") console.group("findAllGlossaryTerms");
        if (debugMode == "true") console.log("Debug Mode ... Error getting parameters list");
        if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
        $("#status").removeClass("warning correct incorrect");
        $("#status").addClass("warning").html(`<b>Error:</b> Parameters were not found`);
        $("#status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
        // *** DEBUG MODE
        if (debugMode == "true") console.group("findAllGlossaryTerms");
        if (debugMode == "true") console.log("Debug Mode ... Function -> Find all paramters End");
        if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
        unlockButton("#selectTermsButton");
        unlockButton("#findAllTerms");
        // ***
        return;
      }
    } else {
      // *** DEBUG MODE
      if (debugMode == "true") console.group("findAllParameter");
      if (debugMode == "true")
        console.trace("Debug Mode ... Error getting information about current configuration context");
      if (debugMode == "true") console.groupEnd("findAllParameter");
      // ***
      $("#connectParametersWithSelected").removeAttr("disabled");
      $(".status").removeClass("warning correct incorrect");
      $("#findAllParameters").removeAttr("disabled");
      $(".status").addClass("warning").html(`<b>Error:</b> Parameters were not found`);
      $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
      // *** DEBUG MODE
      if (debugMode == "true") console.group("findAllParameter");
      if (debugMode == "true") console.trace("Debug Mode ... Function -> Find all paramters End");
      if (debugMode == "true") console.groupEnd("findAllParameter");
      // ***
      unlockAllButtons();
      return;
    }
  });

  // *** DEBUG MODE
  if (debugMode == "true") console.group("findAllGlossaryTerms");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Find all paramters End");
  if (debugMode == "true") console.groupEnd("findAllGlossaryTerms");
  // ***
}

function getParametersURL(componentURL, projectURL, configurationURL) {
  // *** DEBUG MODE
  if (debugMode == "true") console.group("getParametersURL");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Get Parameter URL Start");
  if (debugMode == "true") console.groupEnd("getParametersURL");

  // ***

  var serverURL = projectURL.split("/process/project-areas/")[0];

  var query = serverURL + "/types?resourceContext=" + projectURL;

  var parameter_req = new XMLHttpRequest();
  parameter_req.open("GET", query, false);
  parameter_req.setRequestHeader("Accept", "application/xml");
  parameter_req.setRequestHeader("Content-Type", "application/xml");
  parameter_req.setRequestHeader("OSLC-Core-Version", "2.0");
  parameter_req.setRequestHeader("Configuration-Context", configurationURL);

  // *** DEBUG MODE]
  if (debugMode == "true") console.group("getParametersURL");
  if (debugMode == "true") console.log("Debug Mode ... Getting list of parameters");
  if (debugMode == "true") console.groupEnd("getParametersURL");
  // ***

  parameter_req.send();
  try {
    var objects = parameter_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rdm/rdf/", "ObjectType");

    for (i = 0; i < objects.length; i++) {
      var object = objects[i];

      for (var c = 0; c < object.childNodes.length; c++) {
        if (object.childNodes[c].nodeName == "dcterms:title") {
          if (object.childNodes[c].innerHTML == parameterName) {
            for (var g = 0; g < object.childNodes.length; g++) {
              if (object.childNodes[g].nodeName == "rm:objectTypeRole") {
                var objTmp = object.childNodes[g];

                if (objTmp.getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource")) {
                  var parameterURI = object.getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");
                  // *** DEBUG MODE
                  if (debugMode == "true") console.group("getParametersURL");
                  if (debugMode == "true") console.log("Debug Mode ... Found matching parameter");
                  if (debugMode == "true") console.groupEnd("getParametersURL");
                  // ***
                  // *** DEBUG MODE
                  if (debugMode == "true") console.group("getParametersURL");
                  if (debugMode == "true") console.log("Debug Mode ... Function -> Get Parameter URL End");
                  if (debugMode == "true") console.groupEnd("getParametersURL");
                  // ***

                  return parameterURI;
                }
              }
            }
          }
        }
      }
    }
  } catch (err) {
    // *** DEBUG MODE
    if (debugMode == "true") console.group("getParametersURL");
    if (debugMode == "true") console.log("Debug Mode ... Error getting list of parameters");
    if (debugMode == "true") console.groupEnd("getParametersURL");
    // ***
    unlockAllButtons();
    $(".status").addClass("warning").html(`<b>Error:</b> Parameters were not found`);
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
    // *** DEBUG MODE
    if (debugMode == "true") console.group("getParametersURL");
    if (debugMode == "true") console.log("Debug Mode ... Function -> Get Parameter URL End");
    if (debugMode == "true") console.groupEnd("getParametersURL");
    // ***
    return;
  }

  // *** DEBUG MODE
  if (debugMode == "true") console.group("getParametersURL");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Get Parameter URL End");
  if (debugMode == "true") console.groupEnd("getParametersURL");
  // ***
}

function getProjectAreaURL(componentURL) {
  // *** DEBUG MODE
  if (debugMode == "true") console.group("getProjectAreaURL");
  if (debugMode == "true") console.log("Debug Mode ... Function -> Get Project URL Start");
  if (debugMode == "true") console.groupEnd("getProjectAreaURL");
  // ***

  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", componentURL, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");

  try {
    if (debugMode == "true") console.group("getProjectAreaURL");
    if (debugMode == "true") console.log("Debug Mode ... Getting Project URL");
    if (debugMode == "true") console.groupEnd("getProjectAreaURL");
    projectArea_req.send();

    var project = projectArea_req.responseXML.getElementsByTagNameNS("http://jazz.net/ns/process#", "projectArea");

    var projectURI = project[0].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
    // *** DEBUG MODE
    if (debugMode == "true") console.group("getProjectAreaURL");
    if (debugMode == "true") console.log("Debug Mode ... Function -> Get Project URL End");
    if (debugMode == "true") console.groupEnd("getProjectAreaURL");
    // ***
    return projectURI;
  } catch (err) {
    if (debugMode == "true") console.group("getProjectAreaURL");
    if (debugMode == "true") console.log("Debug Mode ... Error getting Project URL");
    if (debugMode == "true") console.groupEnd("getProjectAreaURL");
    unlockAllButtons();
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("warning").html(`<b>Error:</b> Parameters were not found`);
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
    // *** DEBUG MODE
    if (debugMode == "true") console.group("getProjectAreaURL");
    if (debugMode == "true") console.log("Debug Mode ... Function -> Get Project URL End");
    if (debugMode == "true") console.groupEnd("getProjectAreaURL");
    // ***
    return;
  }
}

async function getAttributes(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getAttributes(
      ref,
      [RM.Data.Attributes.NAME, RM.Data.Attributes.PRIMARY_TEXT, RM.Data.Attributes.IS_HEADING],
      resolve
    );
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

async function createTermLink(source, target) {
  return new Promise(function (resolve, reject) {
    RM.Data.createLink(source, RM.Data.LinkTypes.REFERENCES_TERM, target, resolve);
  });
}

function lockAllButtons() {
  $("#actionButton").attr("disabled", "disabled");
  $("#actionButton").removeClass("btn-primary");
  $("#actionButton").addClass("btn-secondary");
  $("#selectTermsButton").attr("disabled", "disabled");
  $("#selectTermsButton").removeClass("btn-primary");
  $("#selectTermsButton").addClass("btn-secondary");
  $("#findAllTerms").attr("disabled", "disabled");
  $("#findAllTerms").removeClass("btn-primary");
  $("#findAllTerms").addClass("btn-secondary");
  $("#actionInModuleButton").attr("disabled", "disabled");
  $("#actionInModuleButton").removeClass("btn-primary");
  $("#actionInModuleButton").addClass("btn-secondary");
}

function unlockAllButtons() {
  $("#actionButton").removeAttr("disabled");
  $("#actionButton").addClass("btn-primary");
  $("#actionButton").removeClass("btn-secondary");
  $("#selectTermsButton").removeAttr("disabled");
  $("#selectTermsButton").addClass("btn-primary");
  $("#selectTermsButton").removeClass("btn-secondary");
  $("#findAllTerms").removeAttr("disabled");
  $("#findAllTerms").addClass("btn-primary");
  $("#findAllTerms").removeClass("btn-secondary");

  if (moduleRef == null) return;
  $("#actionInModuleButton").removeAttr("disabled");
  $("#actionInModuleButton").addClass("btn-primary");
  $("#actionInModuleButton").removeClass("btn-secondary");
}

function lockButton(button) {
  $(button).attr("disabled", "disabled");
  $(button).removeClass("btn-primary");
  $(button).addClass("btn-secondary");
}

function unlockButton(button) {
  if (button == "#actionInModuleButton" && moduleRef == null) return;
  $(button).removeAttr("disabled");
  $(button).addClass("btn-primary");
  $(button).removeClass("btn-secondary");
}

async function getModuleArtifacts(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getContentsAttributes(ref, [RM.Data.Attributes.PRIMARY_TEXT], resolve);
  });
}

function uriToPublishURI(uri) {
  var tmp = uri.replace("/resources/", "/publish/resources?resourceURI=");
  var url_req = new XMLHttpRequest();
  url_req.open("GET", tmp, false);
  url_req.setRequestHeader("Accept", "application/xml");
  url_req.setRequestHeader("Content-Type", "application/xml");

  try {
    url_req.send();
    var url = url_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rrm/1.0/", "core");
    var newURL = url[0].innerHTML;

    if (newURL) return uri.split("/resources/")[0] + "/resources/" + newURL;
    else return uri;
  } catch (err) {
    return;
  }
}

function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}
